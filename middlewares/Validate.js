
const { ValidationError } = require ("../services/Exceptions")

function ValidateIPaddress(ipaddress) {
	var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
	if (ipaddress.match(ipformat)) return true
	return false
}

exports.ip = (req, res, next) => {
	let { ip } = req.body
	if (!ip) ip = req.query.ip
	if (!ValidateIPaddress(ip)) throw new ValidationError("IPAddress Not Valid")
	next()
}