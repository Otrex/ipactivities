
const IPActivityController = require('../controllers/IPActivityController')
const Validate = require ("../middlewares/Validate")
const Request = require ("../middlewares/Request")
const route = require('express').Router()

route.get('/all', IPActivityController.getAll)
route.get('/analytics', Validate.ip, IPActivityController.get)
route.post('/analytics', Validate.ip, Request.check, IPActivityController.store)

module.exports = route