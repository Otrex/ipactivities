
const { AppError } = require('./core/Exception')

class DBError extends AppError {
	constructor(message, cb) {
		super("DBError")
		this.message = message
		this.statusCode = 500;
		this.log()
	}
}

class RequestError extends AppError {
	constructor(message, statusCode=400) {
		super("RequestError")
		this.message = message
		this.statusCode = statusCode
	}
}

class ValidationError extends AppError {
	constructor(message, statusCode=400) {
		super("ValidationError")
		this.message = message
		this.statusCode = statusCode
	}
}


module.exports = {
	RequestError,
	ValidationError,
	DBError
}