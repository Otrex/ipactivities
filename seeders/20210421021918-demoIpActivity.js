'use strict';
const { random } = require('../utils/tools')

const generateIps = (n)=>{
  let ips = [{ip : "192.168.43.1"}]
  for (var i = 0; i <= n; i++ ){
    ips.push({
        ip: `${random(3)}.${random(3)}.${random(2)}.${random(1)}`,
    })
  }

  return ips
}

const generateCoordinates = (ipids, n) => {
  let coords = []
  ipids.forEach(ipid =>{
    for (var i = 0; i <= n; i++ ){
      coords.push({
        x: random(3), y:random(3), 
        IPActivityId : ipid.id,
        createdAt : new Date(new Date() - (random(undefined, 0,20)/10) * 60 * 60 * 1000),
        updatedAt : new Date(new Date() - (random(undefined, 0,20)/10) * 60 * 60 * 1000)
      })
    }
  })
  return coords
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('IPActivities', generateIps(5), {});

    const ipactivities = await queryInterface.sequelize.query(
      `SELECT id from IPActivities;`
    );

    const ipRows = ipactivities[0];

    return await queryInterface.bulkInsert('Coordinates', generateCoordinates(ipRows, 5), {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('IPActivities', null, {});
    await queryInterface.bulkDelete('Coordinates', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
