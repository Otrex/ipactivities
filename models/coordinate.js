'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Coordinate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Coordinate.belongsTo(models.IPActivity)
    }
  };
  Coordinate.init({
    x: DataTypes.INTEGER,
    y: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Coordinate',
  });
  return Coordinate;
};