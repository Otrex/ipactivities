'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class IPActivity extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      IPActivity.hasMany(models.Coordinate);
    }
  };
  IPActivity.init({
    ip: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'IPActivity',
    timestamps: false,
  });
  return IPActivity;
};